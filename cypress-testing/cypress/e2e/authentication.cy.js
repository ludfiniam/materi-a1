/// <reference types="cypress" />

//Todo : Set This as localStorage token for aunthentivation

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cnlhamVwYXJhIiwiX2lkIjoiNjMzMmI2NjJjYTU1MzcwMDA5MjRhMjQyIiwibmFtZSI6InN1cnlhIG5zIiwiaWF0IjoxNjY0MjkxMzIwLCJleHAiOjE2Njk0NzUzMjB9.p_2TbXnzu-M_KWqyTGVeCM9VyyecmQN_cWPsTh1CZl8'

describe('Basic Desktop Test', () => {

	before(() => {
		cy.then(() =>{
		window.localStorage.setItem('__auth__token', token)			
		})
	})

    beforeEach(() => {
        // bootstrapping external things
        cy.viewport(1280, 720)
        // cy.visit('https://codedamn.com')
    })

    it('Should load playground correctly', () => {
    	cy.visit("https://codedamn.com/playgrounds")
    	cy.contains('[data-testid="playground-html"]').click()
    	cy.wait(20000)
    	cy.contains('div', 'Create Playground').find('button').first().click()
    	cy.get('div');
    	cy.debug();
    })

})
