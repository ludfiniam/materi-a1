/// <reference types="cypress" />

//Todo : Set This as localStorage token for aunthentivation
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cnlhamVwYXJhIiwiX2lkIjoiNjMzMmI2NjJjYTU1MzcwMDA5MjRhMjQyIiwibmFtZSI6InN1cnlhIG5zIiwiaWF0IjoxNjY0MjkxMzIwLCJleHAiOjE2Njk0NzUzMjB9.p_2TbXnzu-M_KWqyTGVeCM9VyyecmQN_cWPsTh1CZl8'


describe('Basic Desktop Test', () => {

	// before(() => {
	// 	cy.then(() =>{
	// 	window.localStorage.setItem('__auth__token', token)			
	// 	})
	// })

    beforeEach(() => {
        // bootstrapping external things
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com')
    })

    it('Basic elements exist', () => {

        // mocha library
        cy.contains('Learn ProgrammingInteractively').should('exist')
        
        cy.get('div#root').should('exist')
        cy.get('[data-testid=logo]').click()
        cy.contains('Videos are so 2008-ish...').should('have.text', 'Videos are so 2008-ish...')
    })

    // it('Basic elements exist on mobile', () => {
         // cy.viewport('iphone-xr')
         //cy.visit('https://codedamn.com')
    // })

    it('The webpage load, page Login', () => {
		cy.contains('Sign in').click()
		cy.contains("Sign in to").should('exist')
		cy.contains('Sign in with Google').should('exist')
		cy.contains('Sign in with GitHub').should('exist')
		cy.contains('Forgot your password?').should('exist')
		cy.contains('Create one').should('exist')
	})

   it('The webpage load, Page forgot password', () => {
		//masuk login
		cy.contains('Sign in').click()
		//masuk halaman forgot your password
		cy.contains('a','Forgot your password?').click({force:true})
		// verifikasi halaman 
		cy.url().should('include', '/password-reset')
		// go back
		cy.go('back')

		cy.contains('Create Free Account').click()
		cy.url().should('include','/register')
	})


	it('Login should not work', () =>{
		cy.contains('Sign in').click()

		cy.contains('Unable to authorize').should('not.exist')

		cy.contains('div','Email address / Username').find('input').first().type('coba@gmail.com',{force:true})
		cy.get('[data-testid="password"]').type('asasad',{force:true})	
		
		cy.get('[data-testid="login"]').click({force:true})

		cy.contains('Unable to authorize').should('exist')


	})

    it('Login should work', () =>{

		cy.contains('Sign in').click()

		cy.contains('div','Email address / Username').find('input').first().type('lud.sn02@gmail.com',{force:true})
		cy.get('[data-testid="password"]').type('uC@SsoPd*z',{force:true})	
		
		cy.get('[data-testid="login"]').click({force:true})

		cy.url().should('include','/dashboard')
	})
   

})
